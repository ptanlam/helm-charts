# Helm Charts

Create a yaml file.

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: argoproj-https-creds
  namespace: argocd
  labels:
    argocd.argoproj.io/secret-type: repo-creds
stringData:
  url: https://gitlab.com/university-senior-project/helm-charts.git
  password: gitlab-username
  username: gitlab-password
```

Create secret in somewhere else (except this folder).

```shell
kubectl apply -f the-above-file.yaml
```

Move to this directory.
Run.

```shell
kubectl apply -f application.yml
```
